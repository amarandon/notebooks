import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.feature as cfeat


def make_basemap(scale="110m", projection=ccrs.Robinson(), extent=None):
    fig = plt.figure(figsize=(16, 7))
    ax = fig.add_subplot(1, 1, 1, projection=projection)

    # generate a basemap with country borders, oceans and coastlines
    ax.add_feature(cfeat.LAND.with_scale(scale))
    ax.add_feature(cfeat.OCEAN.with_scale(scale))
    ax.add_feature(cfeat.COASTLINE.with_scale(scale))
    ax.add_feature(cfeat.BORDERS.with_scale(scale), linestyle='dotted')
    if extent:
        ax.set_extent(extent)
    return fig, ax