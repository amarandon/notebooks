# notebooks

Une série de notebooks Jupyter qui montrent comment lire des données météo et atmosphériques et les visualiser de manière interactive. Le code est écrit en Python et commenté en français.

Exécuter ces notebooks en ligne avec Binder : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/amarandon%2Fnotebooks/master?filepath=Atelier%20observation%20de%20la%20Terre.ipynb)


## Sommaire

- [Démarrer avec Jupyter](IntroJupyter.ipynb)
- [Lecture d'observations météo avec Pandas et MatPlotLib](PandasMatplotlib.ipynb)
- [Interactivité avec ipywidgets](Ipywidgets.ipynb)
- [Tracé de cartes avec Cartopy](Cartopy.ipynb)
- [Introduction à NetCDF et xarray](NetCDF.ipynb) avec des prévisions de pollution aux particules fines
- [Lecture de prévisions météo au format GRIB](ARPEGE_0.5_eccodes.ipynb)
- Lecture de prévisions météo [ARPEGE 0.5](ARPEGE_0.5_xarray.ipynb) et [ARPEGE 0.1](ARPEGE_0.1_xarray.ipynb)
- [Parallélisation simple avec xarray](multicore-grib-processing-xarray.ipynb)
- [Présentation de HoloViz](HoloViz.ipynb)