{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note: this notebook is an updated version of a blog post written for [WeatherForce](htts://weatherforce.org) technical blog, initially [published here](http://tech.weatherforce.org/blog/multicore-grib-processing-xarray/).**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#  Multicore GRIB processing with xarray \n",
    "\n",
    "In today's post, we're going to use [xarray](http://xarray.pydata.org/) multicore capabilities to access a fairly large weather dataset using Python. To illustrate our purpose, we'll look at 15 years of solar radiations provided by the ECMWF. It takes a while to retrieve such a large dataset from the ECMWF, so I'll just give [a direct link to a file with 15 years of data](https://drive.google.com/file/d/1WNFOnPoHa-R6ur9F9KiHYJDNgI49MJvw/view?usp=sharing) that I've previously retrieved (9.5GB GRIB file). While you're downloading this file, read on to understand how you can retrieve your own datasets from the ECMWF."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Retrieving ECMWF data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The ECMWF provides a convenient [Python library to access its dataset](https://software.ecmwf.int/wiki/display/WEBAPI/Access+ECMWF+Public+Datasets). In order to use it you'll need to create an ECMWF account and [install your  key](https://software.ecmwf.int/wiki/display/WEBAPI/Access+ECMWF+Public+Datasets#AccessECMWFPublicDatasets-key). Please read [the instructions on the ECMWF website](https://software.ecmwf.int/wiki/display/WEBAPI/Access+ECMWF+Public+Datasets) for more info on setting up this library.\n",
    "\n",
    "Once the library is properly installed on your system, you can use it to fetch a dataset with a bit of Python code. For instance, in the following snippet of code we request downwards solar radiation for the first 3 months of 2017:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "from ecmwfapi import ECMWFDataServer\n",
    "server = ECMWFDataServer()\n",
    "server.retrieve({\n",
    "    \"class\": \"ei\",\n",
    "    \"dataset\": \"interim\",\n",
    "    \"date\": \"2017-01-01/to/2017-03-31\",\n",
    "    \"expver\": \"1\",\n",
    "    \"grid\": \"0.75/0.75\",\n",
    "    \"levtype\": \"sfc\",\n",
    "    \"param\": \"169.128\",\n",
    "    \"step\": \"3\",\n",
    "    \"stream\": \"oper\",\n",
    "    \"time\": \"12:00:00\",\n",
    "    \"type\": \"fc\",\n",
    "    \"target\": \"surface-solor-radiation-downwards.grib\",\n",
    "})\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that you know how to retrieve datasets to suit your own needs, let's see how we can use xarray to access them."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Accessing data with xarray\n",
    "\n",
    "Hopefully by now you'll have downloaded the dataset and placed it in a folder near your Python script or notebook. Let's open it with xarray and the pynio engine."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import xarray as xr\n",
    "ds = xr.open_dataset(\"data/grib/Surface_solar_radiation_downwards-2002-2016.grib\", engine=\"pynio\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we inspect the variable we're interested in, we can see that it's indexed by four dimensions: time, forecast time, latitude and longitude."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds.SSRD_GDS0_SFC"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's take a look at the first grid we have in this dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "midnight = ds.SSRD_GDS0_SFC[0,0]\n",
    "midnight.initial_time0_hours"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This gives us solar radiation at 00:00. Let's plot it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "midnight.plot(aspect=2,size=5);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's plot the next grid which is for 12:00."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "noon = ds.SSRD_GDS0_SFC[1,0]\n",
    "noon.plot(aspect=2,size=5);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Accessing individual grids of data like this is fairly quick. Now we're going to extract time series spanning 15 years."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Extracting a time series"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So we select a time series of values near a given point as provided by the 3am run:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lat, lon = 43.6, 1.44  # City of Toulouse, France\n",
    "da = ds.SSRD_GDS0_SFC.sel(forecast_time1=\"03:00:00\", g0_lat_2=lat, g0_lon_3=lon,\n",
    "                          method='nearest')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And let's take only values at 12:00 (values with an even index) using a Python slice step:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "da = da[1::2]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that at this point, we haven't actually read any data because xarray loads data lazily, only when it's needed. Now let's plot this time series and measure how much time it takes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "\n",
    "da.plot(aspect=2, size=5);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "27 seconds is not too bad. But if you have a multi-core computer, xarray can do better."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Enabling multicore processing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To parallelize its work, xarray relies on another library named Dask. Let's tell Dask to enable multiprocessing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import dask.multiprocessing\n",
    "dask.config.set(scheduler=\"processes\");  # Enable multicore parallelism"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we're going to split our dataset in chunks - one chunk per CPU core - so that each chunk can be processed in parallel:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import multiprocessing\n",
    "chunk_size = ds.initial_time0_hours.size // multiprocessing.cpu_count()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "chunked_ds = ds.chunk({\"initial_time0_hours\": chunk_size})\n",
    "chunked_da = chunked_ds.SSRD_GDS0_SFC.sel(forecast_time1=\"03:00:00\", g0_lat_2=lat, g0_lon_3=lon,\n",
    "                                          method='nearest')\n",
    "chunked_da = chunked_da[1::2]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's plot our time series again, to observe how faster it is now:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "\n",
    "chunked_da.plot(aspect=2, size=5);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, it is now much faster because it spreads the work over all the available CPU cores.\n",
    "Don't hesitate to use a system monitoring tool such as the `top` command to observe by yourself how xarray spawns a Python process for each of your CPU cores:\n",
    "\n",
    "![top command screen capture](images/xarray-top.png)\n",
    "\n",
    "That's it for today! Thank you for reading and don't forget that you can always [contact us](https://weatherforce.org/contact/) if you need help to bring weather data into your own projects."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
